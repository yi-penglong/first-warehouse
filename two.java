public class Tesat {
    public static void main(String[] args) {

        String md5String = getMD5String("123456");
        System.out.println(md5String);

    }

    public  static String getMD5String(String str){
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(str.getBytes());

            return new BigInteger(1, md5.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}